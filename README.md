# Drools PROXY

NGiNX PROXY app for our drolls backend api

## usage

### ENivronment Variables

* `LISTEN_PORT` - Port to listen on (default:`8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default:`app`)
* `APP_PORT` - Port of the app to forward requests to (default:`9000`)
